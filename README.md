recruitment-testing insideComponents

You have to develop a sidebar. In this sidebar you'll start on the top with an img (you can use any random image found on internet).
Then you'll put 4 categories : A, B, C, D.  

The fourth category is a dropdown. Clicking on `category D` shoes a submenu under the category D.  

This submenu content has to be : "submenu A", "submenu B", "submenu C".
When you click on a category, or a submenu you'll have to display the clicked element on the body.

For the design we expect a width of 250px. We must have 2 different colors for the sidebar and the body background.    
We also want a padding of 21px between the sidebar and the body. All other design details are free.

To make this project you can't use any library or framework.  
Only pure HTML/CSS/Javascript. You can start from the `index.html`
